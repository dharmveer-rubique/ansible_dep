module.exports = {
  apps : [
    {
      name      : 'API-production',
      script    : 'bin/www.js',
      watch     : false,
      instances : 'max',
      exec_mode : "cluster",
      error_file: "/var/log/production/err.log",
      out_file  : "/var/log/production/out.log",
      env: {
        COMMON_VARIABLE: 'true',
        PORT: 3001,
        NODE_ENV: "development"
      },
      env_production: {
        NODE_ENV: 'production',
        PORT: 3001
      },
      env_demo: {
        PORT: 3084,
        NODE_ENV: 'demo'
      }
    }
  ]
}
