module.exports = {
	apps: [{
		name: 'WORKER REACT',
		script: 'worker/Notification.js',
		watch: false,
		instances: 1,
		error_file: "/var/log/react/err.log",
		out_file: "/var/log/react/out.log",
		env: {
			COMMON_VARIABLE: 'true',
			PORT: 3001,
			NODE_ENV: "development"
		},
		env_production: {
			NODE_ENV: 'production',
			PORT: 3001
		},
		env_demo: {
			PORT: 3084,
			NODE_ENV: 'demo'
		},
		env_react: {
			PORT: 3089,
			NODE_ENV: 'react'
		}
	}]
}
