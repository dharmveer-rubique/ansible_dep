module.exports = {
        apps: [{
                name: 'WORKER',
                script: 'worker/Notification.js',
                watch: false,
                instances: 1,
                error_file: "/var/log/production_v4/err.log",
                out_file: "/var/log/production_v4/out.log",
                env: {
                        COMMON_VARIABLE: 'true',
                        PORT: 3001,
                        NODE_ENV: "development"
                },
                env_production: {
                        NODE_ENV: 'production_v4',
                        PORT: 1440
                },
                env_demo: {
                        PORT: 3084,
                        NODE_ENV: 'demo'
                }
        }]
}
