module.exports = {
	apps: [{
		name: 'API',
		script: 'bin/www.js',
		watch: false,
		instances: "1",
		exec_mode: "cluster",
		error_file: "/var/log/demo/err.log",
		out_file: "/var/log/demo/out.log",
		env: {
			COMMON_VARIABLE: 'true',
			PORT: 3001,
			NODE_ENV: "development"
		},
		env_production: {
			NODE_ENV: 'production',
			PORT: 3001
		},
		env_demo: {
			PORT: 3084,
			NODE_ENV: 'demo'
		}
	}]
}
