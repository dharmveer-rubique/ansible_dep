module.exports = {
	apps: [{
		name: 'ICICI_API',
		script: 'bin/www.js',
		watch: false,
		instances: "max",
		exec_mode: "cluster",
		error_file: "/var/log/icici/err.log",
		out_file: "/var/log/icici/out.log",
		env: {
			COMMON_VARIABLE: 'true',
			PORT: 3001,
			NODE_ENV: "development"
		},
		env_production: {
			NODE_ENV: 'production',
			PORT: 3001
		},
        env_icici: {
             PORT: 3001,
             NODE_ENV: "icici"
         },
		env_demo: {
			PORT: 3084,
			NODE_ENV: 'demo'
		}
	}]
}
